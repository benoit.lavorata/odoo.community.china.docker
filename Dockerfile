FROM registry.gitlab.com/benoit.lavorata/odoo.community.docker:13.0
MAINTAINER Benoit Lavorata

User 0

# Set Odoo timezone to China
ENV ODOO_TIMEZONE=Asia/Shanghai

# Set OS timezone to China
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
  && apt-get -y install ttf-wqy-zenhei \
  && sed -i 's/archive\.ubuntu\.com/mirrors.aliyun.com/g' /etc/apt/sources.list \
  && mkdir -p ~/pip  \
  && echo "[global]" > ~/pip/pip.conf \
  && echo "index-url = http://mirrors.aliyun.com/pypi/simple" >> ~/pip/pip.conf \
  && sed -i "s/fonts\.googleapis\.com/fonts.lug.ustc.edu.cn/g" `grep 'fonts\.googleapis\.com' -rl /opt/odoo/sources/odoo/addons`


# Startup script for custom setup
ENTRYPOINT [ "/usr/bin/dumb-init", "/usr/bin/boot.sh" ]
CMD [ "help" ]
EXPOSE 8069 8072
